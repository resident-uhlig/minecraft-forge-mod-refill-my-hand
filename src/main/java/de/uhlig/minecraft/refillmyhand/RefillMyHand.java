package de.uhlig.minecraft.refillmyhand;

import net.minecraft.server.level.ServerPlayer;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.entity.EquipmentSlot;
import net.minecraft.world.item.ItemStack;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.entity.player.PlayerDestroyItemEvent;
import net.minecraftforge.eventbus.api.IEventBus;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;

@Mod("refill_my_hand")
public class RefillMyHand {
    @SuppressWarnings("unused")
    public RefillMyHand() {
        this(MinecraftForge.EVENT_BUS);
    }

    RefillMyHand(final IEventBus eventBusMock) {
        eventBusMock.register(this);
    }

    @SubscribeEvent
    public void refillMyHand(final PlayerDestroyItemEvent event) {
        if (event.getHand() != InteractionHand.MAIN_HAND) {
            return;
        }

        final var original = event.getOriginal().getItem();
        final var player = event.getEntity();
        final var inventory = player.getInventory();
        final var originalIndex = inventory.selected;

        var otherIndex = 0;
        for (final var other : inventory.items) {
            if (otherIndex != originalIndex && original.equals(other.getItem())) {
                inventory.setItem(originalIndex, other);
                inventory.setItem(otherIndex, ItemStack.EMPTY);
                if (player instanceof final ServerPlayer serverPlayer) {
                    serverPlayer.setItemSlot(EquipmentSlot.MAINHAND, other);
                }

                return;
            }

            otherIndex++;
        }
    }
}
