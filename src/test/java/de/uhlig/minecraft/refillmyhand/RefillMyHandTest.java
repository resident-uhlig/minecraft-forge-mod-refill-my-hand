package de.uhlig.minecraft.refillmyhand;

import de.uhlig.minecraft.MinecraftExtension;
import net.minecraft.core.NonNullList;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.entity.EquipmentSlot;
import net.minecraft.world.entity.player.Inventory;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.ItemStack;
import net.minecraftforge.event.entity.player.PlayerDestroyItemEvent;
import net.minecraftforge.eventbus.api.IEventBus;
import net.minecraftforge.registries.ForgeRegistries;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.mockito.Mockito.*;

@ExtendWith({MockitoExtension.class, MinecraftExtension.class})
class RefillMyHandTest {

    @InjectMocks
    private RefillMyHand underTest;

    @Mock
    private IEventBus eventBus;

    @Mock
    private PlayerDestroyItemEvent event;

    @Mock
    private ServerPlayer player;

    @Mock
    private Inventory inventory;

    @Test
    void registerEventBus() {
        // GIVEN

        // WHEN
        // underTest is constructed

        // THEN
        verify(eventBus).register(underTest);
    }

    @Test
    void refill() throws NoSuchFieldException, IllegalAccessException {
        // GIVEN
        when(event.getHand()).thenReturn(InteractionHand.MAIN_HAND);

        final var originalHolder = ForgeRegistries.ITEMS.getHolder(new ResourceLocation("dirt")).orElseThrow();
        final var originalStack = new ItemStack(originalHolder);
        when(event.getOriginal()).thenReturn(originalStack);

        when(event.getEntity()).thenReturn(player);
        when(player.getInventory()).thenReturn(inventory);

        final var originalIndex = 42;
        inventory.selected = originalIndex;

        final var items = NonNullList.withSize(originalIndex + 1, ItemStack.EMPTY);
        setInaccessibleField(inventory, items);

        final var otherStack = new ItemStack(originalHolder);
        final var otherIndex = 0;
        items.set(otherIndex, otherStack);

        // WHEN
        underTest.refillMyHand(event);

        // THEN
        verify(inventory).setItem(originalIndex, otherStack);
        verify(inventory).setItem(otherIndex, ItemStack.EMPTY);
        verify(player).setItemSlot(EquipmentSlot.MAINHAND, otherStack);

        verifyNoMoreInteractions(player, inventory);
    }

    @Test
    void refillWhenNotServerPlayerEntity() throws NoSuchFieldException, IllegalAccessException {
        // GIVEN
        when(event.getHand()).thenReturn(InteractionHand.MAIN_HAND);

        final var originalHolder = ForgeRegistries.ITEMS.getHolder(new ResourceLocation("dirt")).orElseThrow();
        final var originalStack = new ItemStack(originalHolder);
        when(event.getOriginal()).thenReturn(originalStack);

        final var player = mock(Player.class);
        when(event.getEntity()).thenReturn(player);
        when(player.getInventory()).thenReturn(inventory);

        final var originalIndex = 42;
        inventory.selected = originalIndex;

        final var items = NonNullList.withSize(originalIndex + 1, ItemStack.EMPTY);
        setInaccessibleField(inventory, items);

        final var otherStack = new ItemStack(originalHolder);
        final var otherIndex = 0;
        items.set(otherIndex, otherStack);

        // WHEN
        underTest.refillMyHand(event);

        // THEN
        verify(inventory).setItem(originalIndex, otherStack);
        verify(inventory).setItem(otherIndex, ItemStack.EMPTY);

        verifyNoMoreInteractions(player, inventory);
    }

    @Test
    void noRefillWhenDifferentItem() throws NoSuchFieldException, IllegalAccessException {
        // GIVEN
        when(event.getHand()).thenReturn(InteractionHand.MAIN_HAND);

        final var originalHolder = ForgeRegistries.ITEMS.getHolder(new ResourceLocation("dirt")).orElseThrow();
        final var originalStack = new ItemStack(originalHolder);
        when(event.getOriginal()).thenReturn(originalStack);

        when(event.getEntity()).thenReturn(player);
        when(player.getInventory()).thenReturn(inventory);

        final var originalIndex = 42;
        inventory.selected = originalIndex;

        final var items = NonNullList.withSize(originalIndex + 1, ItemStack.EMPTY);
        setInaccessibleField(inventory, items);

        final var grass = ForgeRegistries.ITEMS.getHolder(new ResourceLocation("grass")).orElseThrow();
        final var otherStack = new ItemStack(grass);
        final var otherIndex = 0;
        items.set(otherIndex, otherStack);

        // WHEN
        underTest.refillMyHand(event);

        // THEN
        verifyNoMoreInteractions(player, inventory);
    }

    @Test
    void noRefillWhenSameIndex() throws NoSuchFieldException, IllegalAccessException {
        // GIVEN
        when(event.getHand()).thenReturn(InteractionHand.MAIN_HAND);

        final var originalHolder = ForgeRegistries.ITEMS.getHolder(new ResourceLocation("dirt")).orElseThrow();
        final var originalStack = new ItemStack(originalHolder);
        when(event.getOriginal()).thenReturn(originalStack);

        when(event.getEntity()).thenReturn(player);
        when(player.getInventory()).thenReturn(inventory);

        final var originalIndex = 42;
        inventory.selected = originalIndex;

        final var items = NonNullList.withSize(originalIndex + 1, ItemStack.EMPTY);
        setInaccessibleField(inventory, items);

        final var otherStack = ItemStack.EMPTY;
        final var otherIndex = 0;
        items.set(otherIndex, otherStack);
        items.set(originalIndex, otherStack);

        // WHEN
        underTest.refillMyHand(event);

        // THEN
        verifyNoMoreInteractions(player, inventory);
    }

    @Test
    void noRefillWhenMainInventoryEmpty() throws NoSuchFieldException, IllegalAccessException {
        // GIVEN
        setInaccessibleField(inventory, NonNullList.withSize(0, ItemStack.EMPTY));

        // WHEN
        underTest.refillMyHand(event);

        // THEN
        verifyNoMoreInteractions(player, inventory);
    }

    @Test
    void noRefillWhenNotMainHand() {
        // GIVEN
        when(event.getHand()).thenReturn(InteractionHand.OFF_HAND);

        // WHEN
        underTest.refillMyHand(event);

        // THEN
        verifyNoMoreInteractions(player, inventory);
    }

    private static void setInaccessibleField(final Object target, final Object value) throws NoSuchFieldException, IllegalAccessException {
        final var field = target.getClass().getField("items");
        final var wasAccessible = field.canAccess(target);
        field.setAccessible(true);
        field.set(target, value);
        field.setAccessible(wasAccessible);
    }
}