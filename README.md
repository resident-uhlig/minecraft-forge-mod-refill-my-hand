# Minecraft Forge Mod - Refill My Hand

<a href="https://legacy-code.dev/"><img alt="Legacy Code: O++ S++ !I C E- !M !V D+ !A" src="https://img.shields.io/badge/Legacy%20Code-O%2B%2B%20S%2B%2B%20!I%20C%20E--%20!M%20!V%20D%2B%20!A-informational"></a>
![Minecraft Version: 1.20.1 (Java)](https://img.shields.io/badge/Minecraft_Version-1.20.1_(Java)-blue)

## What it is

This is a client mod for the game [Minecraft](https://www.minecraft.net/).

## What it does

This mod automagically tries to refill the item in the player's main hand with the same item from the inventory or hot bar when the hand becomes empty due to placing a block or breaking a tool.

### Refill after last block was placed

![A hot bar slot becomes empty after placing a block and is refilled](docs/building-blocks.gif)

### Refill after tool breaks

![A hot bar slot becomes empty after breaking a tool and is refilled](docs/breaking-tool.gif)

## How to install it

1. Install Minecraft Forge
    1. Go to the [Minecraft Forge Download Page](http://files.minecraftforge.net/).
    2. Download the installer for version 1.20.1.
    3. Run the installer.
    4. Select "Install client" and the correct Minecraft directory.
    5. Click "OK".
    6. Wait until the installer is finished.
2. Download the mod file [refill_my_hand-0.0.1.jar](https://gitlab.com/resident-uhlig/minecraft-forge-mod-refill-my-hand/-/jobs/artifacts/master/raw/build/libs/refill_my_hand-0.0.1.jar?job=pack)
3. Put the downloaded file in the directory `mods` in your Minecraft directory (same as above). If the directory does not exist, create it.

## How it works

Whenever a player _destroys_ an item, then the player's inventory is scanned for an equivalent item. If such an item is found, it is placed in the same slot as before. If no matching item is found, then the mod does nothing.

_Destroy_ means to either break a tool or to place the last item of a stack.

## Limitations

At the time of writing this has only been tested in single player survival mode.

## Development

### Run locally

```shell
gradlew runClient
```

### Build

The following command creates [refill_my_hand-0.0.1.jar](build/libs/refill_my_hand-0.0.1.jar)

```shell
gradlew build
```

### Test

```shell
gradlew test
```

## License

Please see [LICENSE.md](LICENSE.md).
